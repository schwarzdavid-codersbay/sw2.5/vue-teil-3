import { createRouter, createWebHistory } from 'vue-router'
import HomeView from '@/views/HomeView.vue'
import AboutView from '@/views/AboutView.vue'
import UserDetailView from '@/views/UserDetailView.vue'

const routes = [
  {
    path: '/',
    component: HomeView
  },
  {
    path: '/about',
    component: AboutView
  },
  {
    path: '/user/:userId',
    component: UserDetailView
  }
]

const router = createRouter({
  routes,
  history: createWebHistory()
})

export {
  router
}
